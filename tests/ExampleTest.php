<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testExample()
    {
        $response = $this->call('GET','/');
        var_dump($response->getContent());

        $this->assertEquals("Lumen (5.5.2) (Laravel Components 5.5.*)",$response->getContent());
    }

    public function testAddProduct(){
        $response= $this->call('POST','/products',
            ['name'=>'item5',
             'value'=>'350',
             'tags'=>'mobile,electronics,cover',
             'category_id'=>'1',
             'collection_id'=>'5',
             'published'=>'1']
        );
        var_dump($response->getContent());
        $this->assertEquals("Product Added",$response->getContent());
    }

    public function testSingleItemPlaceOrder(){
        $response= $this->call('POST','/api/v1/orders.json',
            ['endpoint_url'=>'api/v1/orders.json',
                'method'=>'POST',
                'parameters'=>[
                    'order'=>[
                        'order_id'=>51275,
                        'email'=>'test@email.com',
                        'total_amount_net'=>'1890.00',
                        'shipping_costs'=>'29.00',
                        'payment_method'=>'VISA',
                        'items'=>[[
                            'name'=>'Item1',
                            'qnt'=>1,
                            'value'=>1100,
                            'category'=>'Fashion',
                            'subcategory'=>'Jacket',
                            'tags'=>['porsche','design'],
                            'collection_id'=>12

                        ]],
                    ]
                ]
            ]
        );
        var_dump(json_decode($response->getContent(),true));
        $this->assertContains("Order Placed Successfully",json_decode($response->getContent(),true));
    }

    public function testMultiItemsPlaceOrder(){
        $response= $this->call('POST','/api/v1/orders.json',
            ['endpoint_url'=>'api/v1/orders.json',
                'method'=>'POST',
                'parameters'=>[
                    'order'=>[
                        'order_id'=>51275,
                        'email'=>'test@email.com',
                        'total_amount_net'=>'1890.00',
                        'shipping_costs'=>'29.00',
                        'payment_method'=>'VISA',
                        'items'=>[[
                            'name'=>'Item1',
                            'qnt'=>1,
                            'value'=>1100,
                            'category'=>'Fashion',
                            'subcategory'=>'Jacket',
                            'tags'=>['porsche','design'],
                            'collection_id'=>12

                        ],[
                            'name'=>'Item2',
                            'qnt'=>2,
                            'value'=>790,
                            'category'=>'Watches',
                            'subcategory'=>'sport',
                            'tags'=>['watch','porsche','electronics'],
                            'collection_id'=>12

                        ],[
                            'name'=>'Item3',
                            'qnt'=>1,
                            'value'=>555,
                            'category'=>'Watches',
                            'subcategory'=>'sport',
                            'tags'=>['watch','porsche','electronics'],
                            'collection_id'=>5

                        ]]
                    ]
                ]
            ]
        );
        var_dump(json_decode($response->getContent(),true));
        $this->assertContains("Order Placed Successfully",json_decode($response->getContent(),true));
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use App\Models\Setting;
use App\Models\Product;
use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Add Products Service
$router->post('/products','productController@store');

//    Added For Testing
$router->get('/products/{id}','productController@getProduct');

$router->post('/api/v1/orders.json','orderController@order');

$router->get('/test',function(){
    $db = app('db');
    echo "Yes! successfully connected to the DB: " .   $db->getDatabaseName();
});

$router->get('/test/discount',function(){
    $record=Setting::all()->first();
    $str = file_get_contents($record->discount_value_url);
    return substr_count(strip_tags(strtolower($str)),'status');
});
<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 3:16 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderItems;

final class Order extends Model
{
    protected $fillable=[
        'email',
        'total_amount_net',
        'shipping_costs',
        'payment_method',
    ];

    public function orderItems() {
        return $this->belongsTo('App\Models\OrderItems');
    }

}

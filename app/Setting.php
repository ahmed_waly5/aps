<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 3:16 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

final class Setting extends Model
{
    protected $fillable=[
        'discount_value_url',
        'max_discount_value'
    ];
}
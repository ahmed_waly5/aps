<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 3:16 PM
 */

namespace App\Models;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

final class OrderItems extends Model
{
    protected $fillable=[
        'order_id',
        'category_id',
        'qnt',
    ];

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }
    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}
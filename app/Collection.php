<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 3:16 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Setting;

final class Collection extends Model
{
    protected $fillable=[
        'name',
        'has_discount',
        'published',
    ];

    public function hasDiscount(){
        if($this->has_discount==1){
            return true;
        }
    }

//    This function returns number of the occurance of the word status in the url provided
    public function getDiscountValue(){
        $final_discount_value=0;
        $record=Setting::all()->first();
        $str = file_get_contents($record->discount_value_url);
        $discount_value =  substr_count(strip_tags(strtolower($str)),'status');
        $max_discount_value = $record->max_discount_value;
        if($discount_value<= $max_discount_value){
            $final_discount_value = $discount_value;
        }else{
            $final_discount_value =$max_discount_value;
        }
        return $final_discount_value/100;
    }
    public function products() {
        return $this->hasMany('App\Models\Product');
    }
}
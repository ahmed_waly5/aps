<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 6:58 PM
 */

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request) {
        $input = $request->all();

        $product = new Product($input);
        $product=  $product->save();
        return 'Product Added';
    }

//    Added For Testing

    public function getProduct($id){
        $record = Product::findOrFail($id);
//        if($record->collection->hasDiscount()){
//            return "Has Discount";
//        }
        return $record->getValueAfterDiscount();

    }

}
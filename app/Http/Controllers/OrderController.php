<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 6:58 PM
 */

namespace App\Http\Controllers;

use App\Models\Collection;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function order(Request $request){
        $total_amount_net=0;
        $total_discount_value=0;
        $order_response['status']='Empty Order';

        $order=$request->all();
        if(count($order['parameters']['order']['items'])){

            foreach($order['parameters']['order']['items'] as $index => $item){
                $item_calculations=$this->calculateItems($item);
                $total_amount_net+=$item_calculations['item_total_value'];
                $total_discount_value+=$item_calculations['item_discount_value'];
                $order_response['order_id']=$order['parameters']['order']['order_id'];
                $order_response['status']='Order Placed Successfully';
                $order_response['total_amount_net']=$total_amount_net;
                $order_response['item_discount_value']=$total_discount_value;
                $order_response['shipping_costs']=$order['parameters']['order']['shipping_costs'];
                $order_response['email']=$order['parameters']['order']['email'];
            }
        }

         return $order_response;

    }

    private function calculateItems($item){
        $item_calculations['item_total_value']=0;
        $item_calculations['item_discount_value']=0;
        $collection = $this->getCollection($item['collection_id']);

        if($collection->hasDiscount()){
            $discount_value=$collection->getDiscountValue();
            $item_total_value = $item['qnt'] *  ($item['value'] - ($item['value'] * $discount_value));
            $item_calculations['item_discount_value']=$item['qnt'] * $item['value'] * $discount_value ;
        }else{
            $item_total_value = $item['qnt'] *  $item['value'];
            $item_calculations['item_total_value']=$item_total_value;
        }

        $item_calculations['item_total_value']=$item_total_value;
        return $item_calculations;

    }
    private function getCollection($col_id){
        $collection = new Collection();
        $collection= $collection->findOrFail($col_id);
        return $collection;
    }

    //
}
<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 3:16 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;


final class Category extends Model
{
    protected $fillable=[
        'name',
        'category_id',
        'published',
    ];

    public function category() {
        return $this->belongsTo('App\Models\Category');
    }

    public function categories() {
        return $this->hasMany('App\Models\Category');
    }
    public function products() {
        return $this->hasMany('App\Models\Product');
    }
}
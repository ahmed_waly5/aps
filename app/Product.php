<?php
/**
 * Created by PhpStorm.
 * User: Ahmed Waly
 * Date: 10/30/2017
 * Time: 3:16 PM
 */

namespace App\Models;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Model;

final class Product extends Model
{
    protected $fillable=[
        'name',
        'value',
        'tags',
        'category_id',
        'collection_id',
        'published'
    ];
    public function category() {
        return $this->belongsTo('App\Models\Category');
    }
    public function collection() {
        return $this->belongsTo('App\Models\Collection');
    }

    public function getValueAfterDiscount(){
        $value_after_discount='';
        $record=Setting::all()->first();
        $str = file_get_contents($record->discount_value_url);
        $discount_value =  substr_count(strip_tags(strtolower($str)),'status');
        $max_discount_value = $record->max_discount_value;
        if($discount_value<= $max_discount_value){
           $value_after_discount = $this->value - ($this->value * ($discount_value/100));
        }else{
            $value_after_discount =$this->value -($this->value * ($max_discount_value/100));
        }
        return $value_after_discount;
    }
}


-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2017 at 09:52 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parentapsstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `published` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `category_id`, `published`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Fashion', 0, 1, NULL, NULL, NULL),
(2, 'Jacket', 1, 1, NULL, NULL, NULL),
(3, 'Watches', 0, 1, NULL, NULL, NULL),
(4, 'Sport', 3, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `has_discount` tinyint(4) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `name`, `has_discount`, `published`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'collection1', 0, 1, NULL, NULL, NULL),
(2, 'collection2', 0, 1, NULL, NULL, NULL),
(3, 'collection3', 0, 1, NULL, NULL, NULL),
(4, 'collection4', 0, 1, NULL, NULL, NULL),
(5, 'collection5', 0, 1, NULL, NULL, NULL),
(6, 'collection6', 0, 1, NULL, NULL, NULL),
(7, 'collection7', 1, 1, NULL, NULL, NULL),
(8, 'collection8', 0, 1, NULL, NULL, NULL),
(9, 'collection9', 0, 1, NULL, NULL, NULL),
(10, 'collection10', 0, 1, NULL, NULL, NULL),
(11, 'collection11', 0, 1, NULL, NULL, NULL),
(12, 'collection12', 1, 1, NULL, NULL, NULL),
(13, 'collection13', 0, 1, NULL, NULL, NULL),
(14, 'collection14', 0, 1, NULL, NULL, NULL),
(15, 'collection15', 0, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_10_30_131927_create_settings_table', 1),
(2, '2017_10_30_162938_create_prodcu_table', 2),
(3, '2017_10_30_163059_create_categories_table', 2),
(4, '2017_10_30_163122_create_orders_table', 2),
(5, '2017_10_30_163522_create_collections_table', 2),
(6, '2017_10_30_170415_create_order_items_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount_net` int(11) NOT NULL,
  `shipping_costs` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qnt` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `collection_id` int(10) UNSIGNED NOT NULL,
  `published` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `value`, `tags`, `category_id`, `collection_id`, `published`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'item1', '1100', 'porsche,design', 1, 12, 1, NULL, '2017-10-30 18:24:26', '2017-10-30 18:24:26'),
(2, 'item2', '790', 'watch,electronics,design', 1, 7, 1, NULL, '2017-10-30 18:25:44', '2017-10-30 18:25:44'),
(3, 'item3', '5000', 'mobile,electronics', 1, 1, 1, NULL, '2017-10-30 18:26:22', '2017-10-30 18:26:22'),
(4, 'item4', '250', 'mobile,electronics,cover', 1, 5, 1, NULL, '2017-10-30 18:26:41', '2017-10-30 18:26:41'),
(5, 'item5', '', '', 0, 0, 0, NULL, '2017-10-30 20:29:31', '2017-10-30 20:29:31'),
(6, 'item5', '', '', 0, 0, 0, NULL, '2017-10-30 20:30:21', '2017-10-30 20:30:21'),
(7, 'item5', '350', 'mobile,electronics,cover', 1, 5, 1, NULL, '2017-10-30 20:32:16', '2017-10-30 20:32:16'),
(8, 'item5', '350', 'mobile,electronics,cover', 1, 5, 1, NULL, '2017-10-30 20:32:45', '2017-10-30 20:32:45'),
(9, 'item5', '350', 'mobile,electronics,cover', 1, 5, 1, NULL, '2017-10-30 20:33:01', '2017-10-30 20:33:01'),
(10, 'item5', '350', 'mobile,electronics,cover', 1, 5, 1, NULL, '2017-10-30 20:37:05', '2017-10-30 20:37:05'),
(11, 'item5', '350', 'mobile,electronics,cover', 1, 5, 1, NULL, '2017-10-30 20:51:08', '2017-10-30 20:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `discount_value_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max_discount_value` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `discount_value_url`, `max_discount_value`, `created_at`, `updated_at`) VALUES
(1, 'https://developer.github.com/v3/#http-redirects', 25, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
